# symfony-crud

CRUD básico con dos entidades, Segmentos y Restaurantes. 
* Volcado de datos a nuestra BD desde archivos JSON externos mediante Symfony Command
* CRUD completo de Segmentos
* UnitTesting, buscar la máxima cobertura 


Comandos a considerar: 
* Creando las entidades: 
~~~
symfony console make:entity Conference
~~~

* Generando el crud de para la entidad _Segmentos_  
~~~
php bin/console generate:doctrine:crud
~~~
